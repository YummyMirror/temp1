public class UserService {
    public static void main(String[] args) {
        User user = new User.Builder()
                .setName("a")
                .setTeam("b")
                .setAge(100)
                .build();
        System.out.println(user);
    }
}
