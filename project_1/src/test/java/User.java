public class User {
    private String name;
    private String team;
    private int age;

    private User(String name, String team, int age) {
        this.name = name;
        this.team = team;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getTeam() {
        return team;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "User{" + "name = '" + name + "; team = '" + team + "; age = " + age + '}';
    }

    public static class Builder {
        private String name;
        private String team;
        private int age;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setTeam(String team) {
            this.team = team;
            return this;
        }

        public Builder setAge(int age) {
            this.age = age;
            return this;
        }

        public User build() {
            return new User(name, team, age);
        }
    }
}
